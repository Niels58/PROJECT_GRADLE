#!groovy
node {
    wrap([$class: 'AnsiColorBuildWrapper', colorMapName: "vga"]) {
        def gradle = tool 'gradle'
        def imagepush = 'awsproject'
        def ecsenvi= 'dev'
        def clusterName = "${ecsenvi}-${imagepush}-cluster"
        def taskDefinition= "${ecsenvi}-${imagepush}-task-definition"
        def awsid = '085763616923'
        def awsregion = 'us-east-1'
        def imagebaseaws = "${awsid}.dkr.ecr.${awsregion}.amazonaws.com"
        def imagepushaws = "${imagebaseaws}/${imagepush}"
        try {
            stage('Checkout') {
                properties([pipelineTriggers([[$class: 'GitHubPushTrigger'], pollSCM('* * * * *')])])
                checkout scm
            }

            stage('Build') {
                sh "docker rmi -f ${imagepush}:latest ${imagepushaws}:latest || true"
                sh "${gradle}/bin/gradle docker -x test"
                sh "aws --region ${awsregion} ecr create-repository --repository-name ${imagepush} || true"
            }
            stage("Push ECR ${imagebaseaws}") {

//                sh "sed -i -- 's|${imagepush}|${imagepushaws}|g' base.yml"
                sh "eval \$(aws --region ${awsregion} ecr get-login --no-include-email | sed 's|https://||')"
                sh """docker tag ${imagepush}:latest ${imagepushaws}:latest
                      docker push ${imagepushaws}:latest
                   """
//                sh "docker-compose push"
//               docker.withRegistry("https://${imagebaseaws}", "ecr:${awsregion}:aws") {
//                  docker.image("${imagepush}").push('latest')
//               }
//
//                sh "docker tag ${imagepush}:latest ${imagepushaws}:latest"
//                sh "docker-compose push"

            }
            stage("Restart TaskDefinition: ${taskDefinition}"){
//                sh "aws ecs list-tasks --cluster ${ecsenvi}-${ecscluster}-cluster --output json | jq -r \".taskArns[]\" | awk '{print \"aws ecs stop-task --cluster ${ecsenvi}-${ecscluster}-cluster --task \\\"\"\$0\"\\\"\"}' | sh"
                def currTaskDef = sh (
                        returnStdout: true,
                        script:  "                                                              \
                            aws --region ${awsregion} ecs describe-task-definition  --task-definition ${taskDefinition}     \
                                                              --output json                       \
                                                              | egrep 'revision'                  \
                                                              | tr ',' ' '                        \
                                                              | awk '{print \$2}'                 \
                          "
                ).trim()
                def currentTask = sh (
                        returnStdout: true,
                        script:  "                                                              \
                            aws --region ${awsregion} ecs list-tasks  --cluster ${clusterName}  \
                                                --output text                                     \
                                                | egrep 'TASKARNS'                                \
                                                | awk '{print \$2}'                               \
                          "
                ).trim()

                println "currentTask=${currentTask} -- currTaskDef=${currTaskDef}"

                sh "aws --region ${awsregion} ecs stop-task --cluster ${clusterName} --task ${currentTask}"
            }

        } catch (err) {

            throw err
        }

    }
}